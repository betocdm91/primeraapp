//
//  ViewController.swift
//  BullsEye
//
//  Created by Carlos Osorio on 24/4/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var targetLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var roundLAbel: UILabel!
    
    @IBOutlet weak var gameSlider: UISlider!
    
    let gameModel = Game()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameModel.restartGame()

        
    }

    
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        
        gameModel.play(sliderValue: sliderValue)
        
        setValues()
    }
    
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        gameModel.restartGame()
    }
    
    
    @IBAction func infoButtonPressed(_ sender: Any) {
        
    }
    
    
    @IBAction func WinnerButtonPressed(_ sender: Any) {
        if gameModel.score > 100 {
            performSegue(withIdentifier: "IdentifierSegue", sender: self)
        }
    }
    
    func setValues(){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLAbel.text = "\(gameModel.roundGame)"
    }
    
}

